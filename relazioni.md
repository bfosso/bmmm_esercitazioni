# RELAZIONE

:skull: :skull: :skull:  

Ogni studente dovrà presentare una relazione relativa ai laboratori svolti entro **5gg LAVORATIVI**  dalla data in cui è previsto l’esame.  
Esempio: supponiamo che l'esame sia previsto per il 04/02.   
 
|29 Gennaio|30 Gennaio|31 Gennaio|01 Febbraio|02 Febbraio|03 Febbraio|04 Febbraio|
|:---:|:---:|:---:|:---:|:---:|:---:|:---:|
|Mercoledì|Giovedì|Venerdì|Sabato|Domenica|Lunedì|Martedì|
|LAVORATIVO|LAVORATIVO|LAVORATIVO|NON LAVORATIVO|NON LAVORATIVO|LAVORATIVO|**ESAME**|
|**LIMITE ULTIMO PER LA CONSEGNA**|4|3|3|3|2|1|

La relazione dovrà riguardare **tutte** le seguenti tematiche:
* Descrizione della procedura bioinformatica applicata per l’assemblaggio di un genoma batterico corredata da un breve commento dei risultati ottenuti;  
* Descrizione della procedura bioinformatica relativa all’analisi di dati di Metabarcoding e discussione dei dati ottenuti. 
    - Inoltre, va effettuata una ricerca in **PubMed** relativa al ***Fusobacterium nucleatum*** e alla possibile associazione con patologie: selezionarne e discuterne uno.  
    - Nella relazione va indicata la ricerca effettuata su **PubMed** e il PMID del lavoro scelto.  
* Nella tabella sottostante è indicato per ciascuno di voi un gene umano per cui dovrà effettuare la ricerca su **ENSEMBL** e indicare le seguenti informazioni:
    - Simbolo ufficiale del Gene;   
    - Identificativo del Gene;
    - Localizzazione cromosomica comprensiva di coordinate e strand;  
    - Numero di trascritti annotati;
        - Considerando il trascritto codificante proteina più lungo dovete indicare:
            * l'identificativo del trascritto;
            * il numero di Esoni che partecipano alla formazione di quel trascritto;
            * la lunghezza delle regione 5' e 3' UTR e della CDS;
            * la lunghezza della proteina codificata.    
    - Numero di Ortologhi annotati.  
    - Utilizzando la sequenza proteica identificate la presenza di possibile proteine omologhe.


|Nome|Cognome|Gene Name|
|:---:|:---:|:---:|
|Damiano|Amoruso|small nuclear ribonucleoprotein polypeptide C|
|Giulia|Angelini|aminoacylase 1|
|Alessandro|Bastone|membrane spanning 4-domains A18|
|Laura |Bellino|TBC1 domain family member 24|
|Federica|Bisceglia|von Willebrand factor C domain containing 2|
|Roberta|Bucci|TNF alpha induced protein 8 like 3|
|Simona|Buonanno|KN motif and ankyrin repeat domains 3|
|Monica|Campioni|intraflagellar transport 88|
|Giulia|Casamassima|histidine triad nucleotide binding protein 3|
|Assunta|Catino|CD27 molecule|
|Clarisssa|Chirulli|docking protein 4|
|Valentina|Consoli|retinol binding protein 4|
|Lorenza|Cutrone|ELAV like RNA binding protein 1|
|Alessia|Daponte|fibroblast growth factor receptor 1|
|Federica Concetta|Di Noia|chromodomain helicase DNA binding protein 1|
|Ruggiero|Dipaola|heterogeneous nuclear ribonucleoprotein U|
|Maurizio|Fanelli|transmembrane protein 132E|
|Olga|Favale|DnaJ heat shock protein family (Hsp40) member C12|
|Francesca|Filannino|cation channel sperm associated 1|
|Francesco|Ladisa|thromboxane A synthase 1|
|Emanuela|Lavarra|gamma-aminobutyric acid type A receptor subunit rho2|
|Nunzio Simone|Lorito|NmrA like redox sensor 1|
|Sabrina|Luchena|cancer susceptibility 1|
|Camilla|Mandorino|PAT1 homolog 2|
|Anna Maria|Marinelli|RFNG O-fucosylpeptide 3-beta-N-acetylglucosaminyltransferase|
|Elisabetta|Marsala|interleukin 16|
|Antonio|Martino|olfactory receptor family 5 subfamily W member 2|
|Anna |Mastrangelo|RB binding protein 6, ubiquitin ligase|
|Stefano|Mazzeo|ubiquitin specific peptidase 31|
|Miriana|Mazzone|heat shock protein family E (Hsp10) member 1|
|Chiara|Morrone|flavin containing dimethylaniline monoxygenase 3|
|Ignazio|Palermo|synaptojanin 1|
|Chiara|Pellegrino|nuclear receptor 2C2 associated protein|
|Antonio|Radesco|protein phosphatase 2 regulatory subunit B'alpha|
|Martina|Rella|mitochondrial ribosomal protein L13|
|Cinzia|Riccardi|defensin beta 110|
|Andrea|Rizzo|SEC24 homolog B, COPII coat complex component|
|Giovanna|Saracino|family with sequence similarity 177 member A1|
|Serena|Sebastiano|thiamine triphosphatase|
|Giorgia|Tarricone|gelsolin|
|Claudia|Telegrafo|trinucleotide repeat containing adaptor 6B|
|Federico|Tridente|collectin subfamily member 12|
|Desiree|Valente|shieldin complex subunit 3|
|Rossella|Valenzano|FXYD domain containing ion transport regulator 5|
|Antonio Benedetto|Ventura|von Willebrand factor A domain containing 5A|
|Alessandra|Zecchillo|TSPY like 4


[Back to the top](../README.md) 